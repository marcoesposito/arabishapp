class Source(object):
	"""docstring for Source"""
	def __init__(self, name, URL, description=""):
		super(Source, self).__init__()
		self.name = name
		self.URL = URL
		self.description = description
		self.elements = set([])
		