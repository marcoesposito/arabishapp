class Item(object):
	"""docstring for Item"""
	def __init__(self, url=None, text=None, age_range=None, area=None, month=None, year=None, description="", author=None, date=None):
		super(Item, self).__init__()
		self.url = url
		self.text = text
		self.age_range = age_range
		self.area = area
		self.month = month
		self.year = year
		self.description = description
		self.author = author
		self.subitems = set([])
		self.date = date
