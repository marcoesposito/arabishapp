from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import math
from datetime import datetime
from TextManager import *

user = "girardifab@gmail.com"
pwd = "ciaodigossino"

fbhome = "http://www.facebook.com/"

pages = ["arabishapparabish"]


driver = webdriver.Firefox()

# ___________ LOGIN ____________

driver.get(fbhome)

assert "Facebook" in driver.title

elem = driver.find_element_by_id("email")
elem.send_keys(user)
elem = driver.find_element_by_id("pass")
elem.send_keys(pwd)
elem.send_keys(Keys.RETURN)


# ______________________________

fb_tags = {
    "post" : "_427x",
    "postdate" : "_5ptz", #data-utime attr
    "commentslist" : "_7791", #<ul>
    "toplevelcomment" :  "_4eek",
    "cmttopbottom" : "_42ef",
    "commenttop" : "_6qw3",
    "cmtuser" : "_6qw4", #<a>
    "cmttext": "_3l3x", # <span>
    "cmtbottom" : "_6coi _6qw9", #<ul>
    "cmtdate" : "_6qw7", #<a> data-utime

}

tm = TextManager()
for page in pages:
    tm.addFBPage(page, page)
    postsURL = fbhome + page + "/posts"
    driver.get(postsURL)
    # go to facebook page
    # go to posts
    # get visible posts
    posts = driver.find_elements_by_class_name(tags["post"])
    for post in posts:
        post_data = []
        tm.addFBPost(page, post_data)
        date = post.find_element_by_class_name(tags["postdate"])
        epoch_date = date.get_attribute("data-utime")
        # get comments from DOM
        #comments_list = post.find_elements_by_class_name(tags["commentslist"])
        
        comments = post.find_elements_by_class_name(tags["toplevelcomment"])

        for cmt in comments:
            # extract and filter relevant text
            # put text in some place
            cmt_author = cmt.find_element_by_class_name(tags["cmtuser"]).text
            cmt_text = cmt.find_element_by_class_name(tags["cmttext"]).text
            cmt_date = cmt.find_element_by_class_name(tags["cmtdate"])
            cmd_date_abbr = cmt.find_element_by_tag_name("abbr")
            cmt_date_val = cmd_date_abbr.get_attribute("data-utime")
            cmt_date_obj = datetime.fromtimestamp(int(cmt_date_val))
            print (cmt_author + " : " + cmt_text + " @ " + cmt_date_val)
            post_data.append({"user":cmt_author,"text":cmt_text, "date":cmt_date_val})


def getVisiblePostsFromFBPage(pagelink, oldestDate, max=math.inf):
    '''
    retrieves all posts from the facebook page at @pagelink
    which are created after @oldestDate. If max is not specified, retrieves all posts, 
    otherwise gets @max post objects
    @return a list
    '''
    pass

def getCommentsFromFBPost(post):
    '''
    retrieves all comments from the given @post
    @return a list containing all raw information about a post
    '''
    pass

def getRelevantText(comment):
    '''extracts all relevant text from the given @comment.
    removes all links, HTML tags and emoji.
    @returns a string
    '''
    pass
    





# __________ CLOSE DRIVER
driver.close()
