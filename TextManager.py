from Source import Source
from Element import Element
import json

class TextManager(object):
    """An object of this class is responsible of gathering data and text extracted from facebook pages"""

    def __init__(self):
        super(TextManager, self).__init__()
        self.sources = set([])

    def get_source_by_name(self, name):
        for source in self.sources:
            if source["name"] == name:
                return(source)
        return(None)


    def get_source_by_URL(self, URL):
        for source in self.sources:
            if source["URL"] == URL:
                return(source)
        return(None)

    def add_facebook_page_element(self, page_element):
        fb_source = self.get_source_by_URL("https://www.facebook.com/")
        if (fb_source == None):
            fb_source = Source("Facebook", "https://www.facebook.com/", "Facebook website")
        fb_source.elements.add(page_element)

    def add_facebook_page(self, page_URL, page_name):
        page_element = Element(page_URL, page_name)
        self.add_facebook_page_element(page_element)



    def getFBPages(self):
        fb_source = self.get_source_by_URL("https://www.facebook.com/")
        if (fb_source == None):
            return(set([]))
        else:
            return(fb_source.elements)

    def get_facebook_page_by_name(self, name):
        for page in self.getFBPages():
            if page.name == name:
                return(page)
        return(None)

    def print_comments(self):
        for source in self.sources:
            for element in source.elements:
                for item in element.items:
                    if item.text is not None:
                        print(item.text)
                    for subitem in item.subitems:
                        if subitem.text is not None:
                            print("\t" + subitem.text)

    def to_json(self, filename):
        obj = {}
        for source in self.sources:
            source_obj = dict()
            obj[source.name] = source_obj
            source_obj["url"] = source.URL
            source_obj["description"] = source.description
            elements = []
            source_obj["elements"] = elements
            for element in source.elements:
                el_obj = dict()
                elements.append(el_obj)
                el_obj["name"] = element.name
                el_obj["description"] = element.description

                items = []
                el_obj["items"] = items
                for item in element.items:
                    item_obj = dict()
                    item_obj["month"] = item.month
                    item_obj["year"] = item.year
                    item_obj["date"] = item.date
                    item_obj["description"] = item.description
                    items.append(item_obj)
                    if item.text is not None:
                        item_obj["text"] = item.text
                        item_obj["language"] = "unknown"
                    subitems = []
                    item_obj["subitems"] = subitems
                    for subitem in item.subitems:
                        sub_obj = dict()
                        subitems.append(sub_obj)
                        if subitem.text is not None:
                            sub_obj["text"] = subitem.text
                        if subitem.author is not None:
                            sub_obj["author"] = subitem.author
                        sub_obj["month"] = subitem.month
                        sub_obj["year"] = subitem.year
                        sub_obj["date"] = subitem.date
                        sub_obj["description"] = subitem.description

        with open(filename, 'w+') as f:
            json.dump(obj, f, sort_keys=True, indent=4)


  #   def getFBPosts(self, pageName):
        # for page in self.data[0]["data"]:
        # 	if page["name"] == pageName:
        # 		return(page["data"])
        # return(None)


    # def add_facebook_post(self, page_name, post):
    # 	for page in self.data[0]["data"]:
    # 		if page["name"] == pageName:
    # 			page["data"].append(postData)
    # 			return
    # 	return(None)



    # def addFBComment(self, pageName, postID, data):
    # 	for page in self.data[0]["data"]:
    # 		if page["name"] == pageName:
    # 			for post in page["data"]:
    # 				if post["id"] == postId:
    # 					post["data"].append(data)
    # 					return
    # 	return(None)

    # def getFBComments(self, pageName, postId):
    # 	for page in self.data[0]["data"]:
    # 		if page["name"] == pageName:
    # 			for post in page["data"]:
    # 				if post["id"] == postId:
    # 					return(post["data"])
    # 	return(None)

