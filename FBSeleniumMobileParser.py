from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime
from TextManager import *
from Item import Item
import time
import random


class FBSeleniumMobileParser(object):
    """docstring for FBSeleniumParser"""
    chromedriver_path = "/home/marco/Scrivania/arabish/chromedriver"

    fb_home = "http://m.facebook.com/"
    tags = {
        "login" : "_54k8",
        "post": "_3drp",
        "back" : "_6j_c",
        "post_text" : "_5rgt",
        "post_header" : "_4g34",
        "post_comments_link" : "_1j-c",
        "password" : "bl",
        "comment" : "_2b04",
        "comment_author" : "_2b05",
        "comment_body" : "_2b06",
        "story" : "_5msj",
        "show_more_comments" : "_108_",
        "show_answers" : "_4ayk",
        "post_container" : "_5rgr"
    }
    ids = {
        "email":"m_login_email",
        "password" : "u_0_1"
    }


    def __init__(self, user, password, text_manager, oldest_date=None, min_wait=1, max_wait=5, max_posts=100, max_comments=10000):
        super(FBSeleniumMobileParser, self).__init__()
        self.user = user
        self.password = password
        #profile = webdriver.ChromeOptions()
        #profile.set_preference("browser.cache.disk.enable", False)
        #profile.set_preference("browser.cache.memory.enable", False)
        #profile.set_preference("browser.cache.offline.enable", False)
        #profile.set_preference("network.http.use-cache", False)
        self.driver = webdriver.Chrome(self.chromedriver_path)
        self.driver.delete_all_cookies()

        self.tm = text_manager
        self.fb_source = Source("Facebook", self.fb_home, "Facebook website")
        self.tm.sources.add(self.fb_source)
        self.oldest_date = oldest_date
        self.min_wait = min_wait
        self.max_wait = max_wait
        self.max_posts = max_posts
        self.max_comments = max_comments

    def close(self):
        self.driver.close()

    def login(self):
        self.driver.get(self.fb_home)
        assert "Facebook" in self.driver.title
        elem = self.driver.find_element_by_id(self.ids["email"])
        elem.send_keys(self.user)
        time.sleep(2.3)
        pass_cont = self.driver.find_element_by_id(self.ids["password"])
        elems = self.driver.find_elements_by_name("pass")
        elem = elems[0]
        #elem = self.driver.find_element_by_class_name(self.tags["password"])
        elem.send_keys(self.password)
        time.sleep(2.435)
        loginButton = self.driver.find_elements_by_class_name(self.tags["login"])
        loginButton[0].click()

    def get_all_texts_in_pages(self, page_identifiers):
        for page_id in page_identifiers:
            page = Element(page_id, page_id)
            self.fb_source.elements.add(page)
            self.get_all_page_texts(page)

    def get_all_texts_in_page(self, page_id):
        page = Element(page_id, page_id)
        self.fb_source.elements.add(page)
        self.get_all_page_texts(page)

    def get_all_page_texts(self, page):
        visited = set([]) # checked for membership -> set
        posts_url = "{}pg/{}{}".format(self.fb_home,page.URL,"/posts")
        self.wait()
        self.driver.get(posts_url)
        posts = self.driver.find_elements_by_class_name(self.tags["post"])
        #stories = self.driver.find_elements_by_class_name(self.tags["story"])
        stories_urls = self.get_posts_story_url(posts)
        while(True):
            self.wait()
            for story_url in stories_urls:
                self.get_all_post_texts(page, story_url)
                visited.add(story_url)
            #for post_html in posts:
                #self.get_all_post_texts(page, post_html)
            self.driver.get(posts_url)
            self.scroll()
            self.short_wait()
            self.scroll()
            self.short_wait()
            self.scroll()
            new_posts = self.driver.find_elements_by_class_name(self.tags["post"])
            new_stories_urls = self.get_posts_story_url(new_posts)
            if self.stop_conditions(stories_urls, new_stories_urls):
                break
            else:
                #posts = [p in new_posts if p not in posts]
                temp_urls = []
                for new_url in new_stories_urls:
                    if new_url not in visited:# and d < oldest_date:
                        temp_urls.append(new_url)
                if len(temp_urls) == 0:
                    break
                stories_urls = temp_urls


    def get_posts_story_url(self, posts_html):
        urls = []
        for post_html in posts_html:
            #post_text_container = post_html.find_elements_by_class_name(self.tags["post_text"])
            #link_tag = post_text_container[0].find_elements_by_tag_name("a")[0]
            link_tag = post_html.find_elements_by_class_name(self.tags["story"])[0]
            urls.append(link_tag.get_attribute("href"))
        return(urls)



    def stop_conditions(self, posts, new_posts):
        if len(new_posts) <= len(posts): return(True)
        
        # check date of latest post
        # latest = new_posts[0]
        # date = self.html_post_date(latest)
        # if date_obj < self.oldest_date: return(True)
        
        if len(posts) >= self.max_posts: return(True)
        return(False) 

    
    def html_post_date(self, post_html):
        date = post_html.find_element_by_class_name(self.tags["postdate"])
        epoch_date = date.get_attribute("data-utime")
        return(datetime.fromtimestamp(int(epoch_date)))



    def get_all_post_texts(self, page, story_url):
        post = Item()
        page.items.add(post)

        self.wait()
        self.driver.get(story_url)

        post_text_element = self.driver.find_elements_by_class_name(self.tags["post_text"])
        if len(post_text_element) != 0:
            post.text = self.get_post_text(post_text_element[0]) #da controllare!!!


        post.author, post.author_url = self.get_post_author()
        instant = self.get_post_date()
        post.date = str(instant)
        post.month = instant.month
        post.year = instant.year

        post.description = "Post"
        
        self.get_all_comments_texts(post)
    

    def get_post_text(self, post_text_element):
        text = []
        pars = post_text_element.find_elements_by_tag_name("p")
        if len(pars) == 0: return("")
        #pars = span[0].find_elements_by_tag_name("p")

        for par in pars:
            tokens = self.process_text(par.text)
            for token in tokens:
                text.append(token)
        return(" ".join(text))


    def get_post_author(self):
        post_header = self.driver.find_elements_by_class_name(self.tags["post_header"])[0]
        try:
            auth = self.driver.find_element_by_xpath('//*[@id="u_0_p"]/div[1]/header/div[2]/div/div/div[1]/h3/span/strong')
        except:
            auth = self.driver.find_elements_by_xpath('//*[@id="u_0_p"]/div[1]/header/div[2]/div/div/div[1]/h3/strong')
            if len(auth) == 0: return("", "")
            else: auth = auth[0]
        author_a = auth.find_elements_by_tag_name("a")[0]
        ret = (author_a.text, author_a.get_attribute("href"))
        return(ret)

    def get_post_date(self):
        import json
        import datetime
        post = self.driver.find_elements_by_class_name(self.tags["post_container"])[0]
        post_info = post.get_attribute("data-ft")
        post_data = json.loads(post_info)
        id = post_data["page_id"]
        epoch_publish_time = post_data["page_insights"][id]["post_context"]["publish_time"]
        date_obj = datetime.datetime.fromtimestamp(int(epoch_publish_time))
        return(date_obj)


    def get_all_comments_texts(self, post):
        while len(self.driver.find_elements_by_class_name(self.tags["show_more_comments"])) != 0:
            self.wait()
            if len(self.driver.find_elements_by_class_name(self.tags["show_more_comments"])) != 0:
                self.driver.find_elements_by_class_name(self.tags["show_more_comments"])[0].click()
            self.short_wait()
            #show_link = self.driver.find_elements_by_class_name(self.tags["show_more_comments"])

        while len(self.driver.find_elements_by_class_name(self.tags["show_answers"])) != 0:
            self.wait()
            if len(self.driver.find_elements_by_class_name(self.tags["show_answers"])) != 0:
                self.driver.find_elements_by_class_name(self.tags["show_answers"])[0].click()
            self.short_wait()

        '''self.wait()
        answers_link = self.driver.find_elements_by_class_name(self.tags["show_answers"])
        while len(answers_link) != 0:
            for answer_link in answers_link:
                self.wait()
                answer_link.click()
            self.short_wait()
            answers_link = self.driver.find_elements_by_class_name(self.tags["show_answers"])
'''
        comments = self.driver.find_elements_by_class_name(self.tags["comment"])
        for comment_html in comments:
            self.get_comment_texts(post, comment_html)










    def get_comment_texts(self, post, comment_html):
        author = self.get_comment_author(comment_html)
        body_tags = comment_html.find_elements_by_class_name(self.tags["comment_body"])
        if len(body_tags) >0:
            body = body_tags[0]
            text_div = body.find_elements_by_tag_name("div")[1] # first one is the author
            # #assert text_div.get_attribute("data-sigil")=="comment-body"
            text = self.process_text(text_div.text)
            # date = comment_html.find_element_by_class_name(self.tags["cmtdate"])
            #date_abbr = comment_html.find_element_by_tag_name("abbr")
            #date_val = date_abbr.get_attribute("data-utime")
            #date_obj = datetime.fromtimestamp(int(date_val))
            comment = Item(text=text, month=post.month,
                           year=post.year, author=author, description="Comment", date=post.date)

            post.subitems.add(comment)

    def get_comment_author(self, comment_html):
        container= comment_html.find_elements_by_class_name(self.tags["comment_author"])[0]
        author_a = container.find_elements_by_tag_name("a")[0]
        ret = (author_a.text, author_a.get_attribute("href"))
        return(ret)





    def wait(self):
       time.sleep(random.uniform(self.min_wait, self.max_wait))

    def scroll(self):
        last_height = self.driver.execute_script("return document.body.scrollHeight")
        self.driver.execute_script("window.scrollTo(0, {})".format(last_height))

    def short_wait(self):
       time.sleep(random.uniform(0.5, 1.5))

    def process_text(self, text):
        return(text.split())
