from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import datetime
from TextManager import *
from Item import Item
import time


class FBSeleniumParser(object):
    """docstring for FBSeleniumParser"""

    fb_home = "http://www.facebook.com/"
    tags = {
        "post": "_427x",
        "postdate": "_5ptz",  # data-utime attr
        "commentslist": "_7791",  # <ul>
        "toplevelcomment":  "_4eek",
        "cmttopbottom": "_42ef",
        "commenttop": "_6qw3",
        "cmtuser": "_6qw4",  # <a>
        "cmttext": "_3l3x",  # <span>
        "cmtbottom": "_6coi _6qw9",  # <ul>
        "cmtdate": "_6qw7",  # <a> data-utime
    }

    def __init__(self, user, password, text_manager):
        super(FBSeleniumParser, self).__init__()
        self.user = user
        self.password = password

        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.cache.disk.enable", False)
        profile.set_preference("browser.cache.memory.enable", False)
        profile.set_preference("browser.cache.offline.enable", False)
        profile.set_preference("network.http.use-cache", False)
        self.driver = webdriver.Firefox(profile)
        self.driver.delete_all_cookies()

        self.tm = text_manager
        self.fb_source = Source("Facebook", self.fb_home, "Facebook website")
        self.tm.sources.add(self.fb_source)

    def close(self):
        self.driver.close()

    def login(self):
        self.driver.get(self.fb_home)
        assert "Facebook" in self.driver.title
        elem = self.driver.find_element_by_id("email")
        elem.send_keys(self.user)
        time.sleep(2.3)
        elem = self.driver.find_element_by_id("pass")
        elem.send_keys(self.password)
        time.sleep(2.435)
        loginButton = self.driver.find_elements_by_css_selector("input[type=submit]")
        loginButton[0].click()

    def get_all_texts(self, page_identifiers):
        for page_id in page_identifiers:
            page = Element(page_id, page_id)
            self.fb_source.elements.add(page)
            self.get_all_page_texts(page)

    def get_all_page_texts(self, page):
        posts_url = self.fb_home + page.URL + "/posts"
        self.driver.get(posts_url)
        posts = self.driver.find_elements_by_class_name(self.tags["post"])
        for post_html in posts:
            self.get_all_post_texts(page, post_html)

    '''
    Ci si ferma:
    1) se viene trovato un post con data minore del limite specificato
    2) se viene raggiunto il numero di post massimo
    3) se viene raggiunto il numero di commenti massimo
    4) se dopo lo scroll i post rimangono gli stessi (punto fisso)
    '''
    def get_all_post_texts(self, page, post_html):
        post = Item()
        page.items.add(post)
        date = post_html.find_element_by_class_name(self.tags["postdate"])
        epoch_date = date.get_attribute("data-utime")
        date_obj = datetime.fromtimestamp(int(epoch_date))
        post.month = date_obj.month
        post.year = date_obj.year
        post.description = "Post"
        comments = post_html.find_elements_by_class_name(self.tags["toplevelcomment"])

        for comment_html in comments:
            self.get_all_comment_texts(post, comment_html)

    def get_all_comment_texts(self, post, comment_html):
        # extract and filter relevant text
        # put text in some place
        author = comment_html.find_element_by_class_name(self.tags["cmtuser"]).text
        text = comment_html.find_element_by_class_name(self.tags["cmttext"]).text
        # date = comment_html.find_element_by_class_name(self.tags["cmtdate"])
        date_abbr = comment_html.find_element_by_tag_name("abbr")
        date_val = date_abbr.get_attribute("data-utime")
        date_obj = datetime.fromtimestamp(int(date_val))
        comment = Item(text=text, month=date_obj.month,
                       year=date_obj.year, author=author, description="Comment")

        post.subitems.add(comment)
