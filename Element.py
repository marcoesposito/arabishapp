class Element(object):
	"""docstring for Element"""
	def __init__(self, URL, name, year=None, month=None, description=""):
		super(Element, self).__init__()
		self.name = name
		self.URL = URL
		self.year = year
		self.month = month
		self.description = description
		self.items = set([])
		